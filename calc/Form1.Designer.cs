﻿namespace calc
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.Calc = new System.Windows.Forms.TextBox();
            this.One_btn = new System.Windows.Forms.Button();
            this.Two_btn = new System.Windows.Forms.Button();
            this.Three_btn = new System.Windows.Forms.Button();
            this.Four_btn = new System.Windows.Forms.Button();
            this.Five_btn = new System.Windows.Forms.Button();
            this.Six_btn = new System.Windows.Forms.Button();
            this.Division_btn = new System.Windows.Forms.Button();
            this.Multiplication_btn = new System.Windows.Forms.Button();
            this.Minus_btn = new System.Windows.Forms.Button();
            this.Plus_btn = new System.Windows.Forms.Button();
            this.Seven_btn = new System.Windows.Forms.Button();
            this.Eight_btn = new System.Windows.Forms.Button();
            this.Nine_btn = new System.Windows.Forms.Button();
            this.Zero_btn = new System.Windows.Forms.Button();
            this.Btn_Clear = new System.Windows.Forms.Button();
            this.Bs_Btn = new System.Windows.Forms.Button();
            this.Calc_flow = new System.Windows.Forms.TextBox();
            this.AllClear_btn = new System.Windows.Forms.Button();
            this.Equal_btn = new System.Windows.Forms.Button();
            this.mod_btn = new System.Windows.Forms.Button();
            this.Formula_lbl = new System.Windows.Forms.Label();
            this.Point_btn = new System.Windows.Forms.Button();
            this.Result_Box = new System.Windows.Forms.TextBox();
            this.result_lbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Calc
            // 
            this.Calc.BackColor = System.Drawing.SystemColors.Window;
            this.Calc.Location = new System.Drawing.Point(12, 49);
            this.Calc.Multiline = true;
            this.Calc.Name = "Calc";
            this.Calc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Calc.Size = new System.Drawing.Size(399, 64);
            this.Calc.TabIndex = 0;
            // 
            // One_btn
            // 
            this.One_btn.Location = new System.Drawing.Point(12, 322);
            this.One_btn.Name = "One_btn";
            this.One_btn.Size = new System.Drawing.Size(75, 75);
            this.One_btn.TabIndex = 1;
            this.One_btn.Text = "1";
            this.One_btn.UseVisualStyleBackColor = true;
            this.One_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Two_btn
            // 
            this.Two_btn.Location = new System.Drawing.Point(93, 322);
            this.Two_btn.Name = "Two_btn";
            this.Two_btn.Size = new System.Drawing.Size(75, 75);
            this.Two_btn.TabIndex = 2;
            this.Two_btn.Text = "2";
            this.Two_btn.UseVisualStyleBackColor = true;
            this.Two_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Three_btn
            // 
            this.Three_btn.Location = new System.Drawing.Point(174, 322);
            this.Three_btn.Name = "Three_btn";
            this.Three_btn.Size = new System.Drawing.Size(75, 75);
            this.Three_btn.TabIndex = 3;
            this.Three_btn.Text = "3";
            this.Three_btn.UseVisualStyleBackColor = true;
            this.Three_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Four_btn
            // 
            this.Four_btn.Location = new System.Drawing.Point(12, 241);
            this.Four_btn.Name = "Four_btn";
            this.Four_btn.Size = new System.Drawing.Size(75, 75);
            this.Four_btn.TabIndex = 4;
            this.Four_btn.Text = "4";
            this.Four_btn.UseVisualStyleBackColor = true;
            this.Four_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Five_btn
            // 
            this.Five_btn.Location = new System.Drawing.Point(93, 241);
            this.Five_btn.Name = "Five_btn";
            this.Five_btn.Size = new System.Drawing.Size(75, 75);
            this.Five_btn.TabIndex = 5;
            this.Five_btn.Text = "5";
            this.Five_btn.UseVisualStyleBackColor = true;
            this.Five_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Six_btn
            // 
            this.Six_btn.Location = new System.Drawing.Point(174, 241);
            this.Six_btn.Name = "Six_btn";
            this.Six_btn.Size = new System.Drawing.Size(75, 75);
            this.Six_btn.TabIndex = 6;
            this.Six_btn.Text = "6";
            this.Six_btn.UseVisualStyleBackColor = true;
            this.Six_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Division_btn
            // 
            this.Division_btn.Location = new System.Drawing.Point(255, 160);
            this.Division_btn.Name = "Division_btn";
            this.Division_btn.Size = new System.Drawing.Size(75, 75);
            this.Division_btn.TabIndex = 7;
            this.Division_btn.Text = "/";
            this.Division_btn.UseVisualStyleBackColor = true;
            this.Division_btn.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // Multiplication_btn
            // 
            this.Multiplication_btn.Location = new System.Drawing.Point(255, 241);
            this.Multiplication_btn.Name = "Multiplication_btn";
            this.Multiplication_btn.Size = new System.Drawing.Size(75, 75);
            this.Multiplication_btn.TabIndex = 8;
            this.Multiplication_btn.Text = "*";
            this.Multiplication_btn.UseVisualStyleBackColor = true;
            this.Multiplication_btn.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // Minus_btn
            // 
            this.Minus_btn.Location = new System.Drawing.Point(255, 322);
            this.Minus_btn.Name = "Minus_btn";
            this.Minus_btn.Size = new System.Drawing.Size(75, 75);
            this.Minus_btn.TabIndex = 9;
            this.Minus_btn.Text = "-";
            this.Minus_btn.UseVisualStyleBackColor = true;
            this.Minus_btn.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // Plus_btn
            // 
            this.Plus_btn.Location = new System.Drawing.Point(255, 403);
            this.Plus_btn.Name = "Plus_btn";
            this.Plus_btn.Size = new System.Drawing.Size(75, 75);
            this.Plus_btn.TabIndex = 10;
            this.Plus_btn.Text = "+";
            this.Plus_btn.UseVisualStyleBackColor = true;
            this.Plus_btn.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // Seven_btn
            // 
            this.Seven_btn.Location = new System.Drawing.Point(12, 160);
            this.Seven_btn.Name = "Seven_btn";
            this.Seven_btn.Size = new System.Drawing.Size(75, 75);
            this.Seven_btn.TabIndex = 11;
            this.Seven_btn.Text = "7";
            this.Seven_btn.UseVisualStyleBackColor = true;
            this.Seven_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Eight_btn
            // 
            this.Eight_btn.Location = new System.Drawing.Point(93, 160);
            this.Eight_btn.Name = "Eight_btn";
            this.Eight_btn.Size = new System.Drawing.Size(75, 75);
            this.Eight_btn.TabIndex = 12;
            this.Eight_btn.Text = "8";
            this.Eight_btn.UseVisualStyleBackColor = true;
            this.Eight_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Nine_btn
            // 
            this.Nine_btn.Location = new System.Drawing.Point(174, 160);
            this.Nine_btn.Name = "Nine_btn";
            this.Nine_btn.Size = new System.Drawing.Size(75, 75);
            this.Nine_btn.TabIndex = 13;
            this.Nine_btn.Text = "9";
            this.Nine_btn.UseVisualStyleBackColor = true;
            this.Nine_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Zero_btn
            // 
            this.Zero_btn.Location = new System.Drawing.Point(12, 403);
            this.Zero_btn.Name = "Zero_btn";
            this.Zero_btn.Size = new System.Drawing.Size(75, 75);
            this.Zero_btn.TabIndex = 14;
            this.Zero_btn.Text = "0";
            this.Zero_btn.UseVisualStyleBackColor = true;
            this.Zero_btn.Click += new System.EventHandler(this.Number_Click);
            // 
            // Btn_Clear
            // 
            this.Btn_Clear.Location = new System.Drawing.Point(336, 241);
            this.Btn_Clear.Name = "Btn_Clear";
            this.Btn_Clear.Size = new System.Drawing.Size(75, 75);
            this.Btn_Clear.TabIndex = 15;
            this.Btn_Clear.Text = "Clear_button";
            this.Btn_Clear.UseVisualStyleBackColor = true;
            this.Btn_Clear.Click += new System.EventHandler(this.Btn_Clear_Click);
            // 
            // Bs_Btn
            // 
            this.Bs_Btn.Location = new System.Drawing.Point(336, 159);
            this.Bs_Btn.Name = "Bs_Btn";
            this.Bs_Btn.Size = new System.Drawing.Size(75, 76);
            this.Bs_Btn.TabIndex = 16;
            this.Bs_Btn.Text = "BackSpace";
            this.Bs_Btn.UseVisualStyleBackColor = true;
            this.Bs_Btn.Click += new System.EventHandler(this.Bs_Btn_Click);
            // 
            // Calc_flow
            // 
            this.Calc_flow.Location = new System.Drawing.Point(12, 24);
            this.Calc_flow.Name = "Calc_flow";
            this.Calc_flow.Size = new System.Drawing.Size(399, 19);
            this.Calc_flow.TabIndex = 17;
            // 
            // AllClear_btn
            // 
            this.AllClear_btn.Location = new System.Drawing.Point(336, 322);
            this.AllClear_btn.Name = "AllClear_btn";
            this.AllClear_btn.Size = new System.Drawing.Size(75, 75);
            this.AllClear_btn.TabIndex = 18;
            this.AllClear_btn.Text = "AllClear";
            this.AllClear_btn.UseVisualStyleBackColor = true;
            this.AllClear_btn.Click += new System.EventHandler(this.AllClear_btn_Click);
            // 
            // Equal_btn
            // 
            this.Equal_btn.Location = new System.Drawing.Point(174, 403);
            this.Equal_btn.Name = "Equal_btn";
            this.Equal_btn.Size = new System.Drawing.Size(75, 75);
            this.Equal_btn.TabIndex = 19;
            this.Equal_btn.Text = "=";
            this.Equal_btn.UseVisualStyleBackColor = true;
            this.Equal_btn.Click += new System.EventHandler(this.Equal_btn_Click);
            // 
            // mod_btn
            // 
            this.mod_btn.Location = new System.Drawing.Point(337, 404);
            this.mod_btn.Name = "mod_btn";
            this.mod_btn.Size = new System.Drawing.Size(75, 74);
            this.mod_btn.TabIndex = 20;
            this.mod_btn.Text = "%";
            this.mod_btn.UseVisualStyleBackColor = true;
            this.mod_btn.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // Formula_lbl
            // 
            this.Formula_lbl.AutoSize = true;
            this.Formula_lbl.Location = new System.Drawing.Point(12, 9);
            this.Formula_lbl.Name = "Formula_lbl";
            this.Formula_lbl.Size = new System.Drawing.Size(41, 12);
            this.Formula_lbl.TabIndex = 21;
            this.Formula_lbl.Text = "計算式";
            // 
            // Point_btn
            // 
            this.Point_btn.Location = new System.Drawing.Point(94, 403);
            this.Point_btn.Name = "Point_btn";
            this.Point_btn.Size = new System.Drawing.Size(75, 75);
            this.Point_btn.TabIndex = 22;
            this.Point_btn.Text = ".";
            this.Point_btn.UseVisualStyleBackColor = true;
            this.Point_btn.Click += new System.EventHandler(this.Point_btn_Click);
            // 
            // Result_Box
            // 
            this.Result_Box.Location = new System.Drawing.Point(255, 120);
            this.Result_Box.Multiline = true;
            this.Result_Box.Name = "Result_Box";
            this.Result_Box.Size = new System.Drawing.Size(156, 33);
            this.Result_Box.TabIndex = 23;
            // 
            // result_lbl
            // 
            this.result_lbl.AutoSize = true;
            this.result_lbl.Location = new System.Drawing.Point(213, 120);
            this.result_lbl.Name = "result_lbl";
            this.result_lbl.Size = new System.Drawing.Size(38, 12);
            this.result_lbl.TabIndex = 24;
            this.result_lbl.Text = "Result";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 507);
            this.Controls.Add(this.result_lbl);
            this.Controls.Add(this.Result_Box);
            this.Controls.Add(this.Point_btn);
            this.Controls.Add(this.Formula_lbl);
            this.Controls.Add(this.mod_btn);
            this.Controls.Add(this.Equal_btn);
            this.Controls.Add(this.AllClear_btn);
            this.Controls.Add(this.Calc_flow);
            this.Controls.Add(this.Bs_Btn);
            this.Controls.Add(this.Btn_Clear);
            this.Controls.Add(this.Zero_btn);
            this.Controls.Add(this.Nine_btn);
            this.Controls.Add(this.Eight_btn);
            this.Controls.Add(this.Seven_btn);
            this.Controls.Add(this.Plus_btn);
            this.Controls.Add(this.Minus_btn);
            this.Controls.Add(this.Multiplication_btn);
            this.Controls.Add(this.Division_btn);
            this.Controls.Add(this.Six_btn);
            this.Controls.Add(this.Five_btn);
            this.Controls.Add(this.Four_btn);
            this.Controls.Add(this.Three_btn);
            this.Controls.Add(this.Two_btn);
            this.Controls.Add(this.One_btn);
            this.Controls.Add(this.Calc);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Calc;
        private System.Windows.Forms.Button One_btn;
        private System.Windows.Forms.Button Two_btn;
        private System.Windows.Forms.Button Three_btn;
        private System.Windows.Forms.Button Four_btn;
        private System.Windows.Forms.Button Five_btn;
        private System.Windows.Forms.Button Six_btn;
        private System.Windows.Forms.Button Division_btn;
        private System.Windows.Forms.Button Multiplication_btn;
        private System.Windows.Forms.Button Minus_btn;
        private System.Windows.Forms.Button Plus_btn;
        private System.Windows.Forms.Button Seven_btn;
        private System.Windows.Forms.Button Eight_btn;
        private System.Windows.Forms.Button Nine_btn;
        private System.Windows.Forms.Button Zero_btn;
        private System.Windows.Forms.Button Btn_Clear;
        private System.Windows.Forms.Button Bs_Btn;
        private System.Windows.Forms.TextBox Calc_flow;
        private System.Windows.Forms.Button AllClear_btn;
        private System.Windows.Forms.Button Equal_btn;
        private System.Windows.Forms.Button mod_btn;
        private System.Windows.Forms.Label Formula_lbl;
        private System.Windows.Forms.Button Point_btn;
        private System.Windows.Forms.TextBox Result_Box;
        private System.Windows.Forms.Label result_lbl;
    }
}


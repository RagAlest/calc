﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace calc
{
    public partial class Form1 : Form
    {
        public double current_fomula;
        public double current_result;
        public double received_fomula;
        public double temp_number;
        public int Bs_count = 0;//boolでもいいよね！
        public int count = 0;
        public bool equal = false;
        public bool operatorPressed = false;
        public int calc_mode = -1;

        public Form1()
        {
            InitializeComponent();
            Calc.Text = "0";
            Calc.TextAlign = HorizontalAlignment.Right;
            Calc_flow.Text = "0";
            Calc_flow.TextAlign = HorizontalAlignment.Right;
            Calc.ReadOnly = true;
            Calc.BackColor = SystemColors.Window;
            Calc_flow.ReadOnly = true;
            Calc_flow.BackColor = SystemColors.Window;
        }

        private void Number_Click(object sender, EventArgs e)
        {
            //数字ボタンクリック->数字表示
            this.Calc.Text = double.Parse(this.Calc.Text + ((Button)sender).Text).ToString();
            operatorPressed = false;
        }

        private void Point_btn_Click(object sender, EventArgs e)
        {
            if (Calc.Text.Contains(".")) return;
            Calc.AppendText(".");
        }

        #region 消去系ボタンクリック

        private void Bs_Btn_Click(object sender, EventArgs e)
        {
            try
            {
                if (Calc.Text.Length == 0)
                {
                    try
                    {
                        Calc_flow.Text = Calc_flow.Text.Remove(Calc_flow.Text.Length - 1, 1);
                        operatorPressed = false;
                    }
                    catch { }
                }
                Calc.Text = Calc.Text.Remove(Calc.Text.Length - 1, 1);
                //String.Removeメソッドは削除した結果の文字列を返すだけで、基の文字列は変更しません。
                //https://dobon.net/vb/dotnet/string/remove.html
            }
            catch { }
            ++Bs_count;
            if (Bs_count > 0) operatorPressed = false;
        }

        private void Btn_Clear_Click(object sender, EventArgs e)
        {
            try
            {
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
        }

        private void AllClear_btn_Click(object sender, EventArgs e)
        {
            try
            {
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
            try
            {
                Calc_flow.Text = Calc_flow.Text.Remove(0);
                Result_Box.Text = Result_Box.Text.Remove(0);
            }
            catch { }
            current_fomula = 0;
            current_result = 0;
            received_fomula = 0;
            temp_number = 0;
            count = 0;
            equal = false;
            operatorPressed = false;
            calc_mode = -1;
            Bs_count = 0;
            Calc.Text = "0";
            Calc_flow.Text = "0";
        }

        #endregion

        #region 演算子ボタンクリック

        private void OperatorClicked(object sender, EventArgs e)//四則演算関数化
        {
            bool alreadyCalc = false;
            //calc_modeの指定をする
            switch (((Button)sender).Text)
            {
                case "+":
                    calc_mode = 0;
                    break;
                case "-":
                    calc_mode = 1;
                    break;
                case "*":
                    calc_mode = 2;
                    break;
                case "/":
                    calc_mode = 3;
                    break;
                case "%":
                    calc_mode = 4;
                    break;
                default:
                    break;
            }
            if (equal == true)
            {
                Calc_flow.Text = "";
                Calc_flow.Text = Convert.ToString(current_result);
                alreadyCalc = true;
            }
            try
            {
                //初回起動時向け。計算式表示(Calc_flow.Text)フォームの先頭の数字が0であれば、計算式表示文字列(Calc_flow.Text)を空にする。
                System.Globalization.StringInfo info = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_alpha = info.SubstringByTextElements(0);
                if (calc_flow_alpha == "0") Calc_flow.Text = "";
            }
            catch { }
            /*try
            {//末尾の演算子が + でなければ、+を追加する
                System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_beta = si.SubstringByTextElements(Calc_flow.Text.Length - 1);//-1でなくした,不要処理の可能性
                switch (calc_mode)
                {
                    case 0:
                        if (calc_flow_beta != "+") Calc_flow.Text += "+";
                        break;
                    case 1:
                        if (calc_flow_beta != "-") Calc_flow.Text += "-";
                        break;
                    case 2:
                        if (calc_flow_beta != "*") Calc_flow.Text += "*";
                        break;
                    case 3:
                        if (calc_flow_beta != "/") Calc_flow.Text += "/";
                        break;
                    case 4:
                        if (calc_flow_beta != "%") Calc_flow.Text += "%";
                        break;
                    default:
                        break;
                }
            }
            catch { }*/
            //演算子2重入力防止
            if (operatorPressed == true) return;
            //0 = 加算
            //何も入力されていない時は何もしない
            if (Calc_flow.Text.Length == 0 && Calc.Text.Length == 0)
            {
                return;
            }
            if (Calc_flow.Text.Length != 0)
            {
                try
                {
                    if (alreadyCalc == false)
                    {
                        //初回の計算ではないとき、かつ、ボタン押下時に計算されていないとき,Calc_flowを初期化&Calc_flow初期化前のもの+現在の入力を追加
                        string temp = Calc_flow.Text;
                        Calc_flow.Text = Calc_flow.Text.Remove(0);
                        Calc_flow.Text = temp + Calc.Text;
                    }
                }
                catch { }
            }
            try
            {
                //ここで、- -> BS -> +  や * -> BS -> + などの操作をすると例外が発生する。そのような操作がされている場合は、catch文にて演算子を表示してこのメソッドの動作は完了する
                received_fomula = Convert.ToDouble(Calc.Text);
                //Calc_flow.Text = Convert.ToString(Convert.ToDouble(Calc_flow.Text));
                if (Calc_flow.Text.Length == 0)
                {
                    //初回入力時、そのまま代入しておわり
                    current_result = received_fomula;
                    switch (calc_mode)
                    {
                        case 0:
                            if (alreadyCalc == false) Calc_flow.Text += Calc.Text + "+";
                            break;
                        case 1:
                            if (alreadyCalc == false) Calc_flow.Text += Calc.Text + "-";
                            break;
                        case 2:
                            if (alreadyCalc == false) Calc_flow.Text += Calc.Text + "*";
                            break;
                        case 3:
                            if (alreadyCalc == false) Calc_flow.Text += Calc.Text + "/";
                            break;
                        case 4:
                            if (alreadyCalc == false) Calc_flow.Text += Calc.Text + "%";
                            break;
                        default:
                            break;
                    }

                }
                else if (equal == false)
                {
                    //初回ではないとき、ここで加算する
                    current_result += received_fomula;
                    switch (calc_mode)
                    {
                        case 0:
                            Calc_flow.Text += "+";
                            break;
                        case 1:
                            Calc_flow.Text += "-";
                            break;
                        case 2:
                            Calc_flow.Text += "*";
                            break;
                        case 3:
                            Calc_flow.Text += "/";
                            break;
                        case 4:
                            Calc_flow.Text += "%";
                            break;
                        default:
                            break;
                    }
                }
            }
            catch
            {
                //演算子再入力時,実行する
                switch (calc_mode)
                {
                    case 0:
                        Calc_flow.Text += "+";
                        break;
                    case 1:
                        Calc_flow.Text += "-";
                        break;
                    case 2:
                        Calc_flow.Text += "*";
                        break;
                    case 3:
                        Calc_flow.Text += "/";
                        break;
                    case 4:
                        Calc_flow.Text += "%";
                        break;
                    default:
                        break;
                }
            }
            try
            {
                //演算子入力後はクリアする
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
            equal = false;
            operatorPressed = true;
            count = 0;
        }

        /*
        private void Plus_btn_Click(object sender, EventArgs e)
        {
            bool alreadyCalc = false;
            calc_mode = 0;
            if (equal == true)
            {
                Calc_flow.Text = "";
                Calc_flow.Text = Convert.ToString(current_result);
                alreadyCalc = true;
            }
            try
            {
                //初回起動時向け。計算式表示(Calc_flow.Text)フォームの先頭の数字が0であれば、計算式表示文字列(Calc_flow.Text)を空にする。
                System.Globalization.StringInfo info = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_alpha = info.SubstringByTextElements(0);
                if (calc_flow_alpha == "0") Calc_flow.Text = "";
            }
            catch { }
            try
            {//末尾の演算子が + でなければ、+を追加する
                System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_beta = si.SubstringByTextElements(Calc_flow.Text.Length - 1);//-1でなくした
                if (calc_flow_beta != "+") Calc_flow.Text += "+";
            }
            catch { }
            //演算子2重入力防止
            if (operatorPressed == true) return;
            //0 = 加算
            //何も入力されていない時は何もしない
            if (Calc_flow.Text.Length == 0 && Calc.Text.Length == 0)
            {
                return;
            }
            if (Calc_flow.Text.Length != 0)
            {
                try
                {
                    if(alreadyCalc == false)
                    {
                        //初回の計算ではないとき、かつ、ボタン押下時に計算されていないとき,Calc_flowを初期化&Calc_flow初期化前のもの+現在の入力を追加
                        string temp = Calc_flow.Text;
                        Calc_flow.Text = Calc_flow.Text.Remove(0);
                        Calc_flow.Text = temp + Calc.Text;
                    }
                }
                catch { }
            }
            try
            {
                //ここで、- -> BS -> +  や * -> BS -> + などの操作をすると例外が発生する。そのような操作がされている場合は、catch文にて演算子を表示してこのメソッドの動作は完了する
                received_fomula = Convert.ToDouble(Calc.Text);
                //Calc_flow.Text = Convert.ToString(Convert.ToDouble(Calc_flow.Text));
                if (Calc_flow.Text.Length == 0)
                {
                    //初回入力時、そのまま代入しておわり
                    current_result = received_fomula;
                    if(alreadyCalc == false)Calc_flow.Text += Calc.Text + "+";
                }
                else if (equal == false)
                {
                    //初回ではないとき、ここで加算する
                    current_result += received_fomula;
                    Calc_flow.Text += "+";
                }
            }
            catch
            {
                //演算子再入力時,実行する
                Calc_flow.Text += "+";
            }
            try
            {
                //演算子入力後はクリアする
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
            equal = false;
            operatorPressed = true;
            count = 0;
        }

        private void Minus_btn_Click(object sender, EventArgs e)
        {
            bool alreadyCalc = false;
            calc_mode = 1;
            if (equal == true)
            {
                Calc_flow.Text = "";
                Calc_flow.Text = Convert.ToString(current_result);
                alreadyCalc = true;
            }
            try
            {
                //初回起動時向け。計算式表示(Calc_flow.Text)フォームの先頭の数字が0であれば、計算式表示文字列(Calc_flow.Text)を空にする。
                System.Globalization.StringInfo info = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_alpha = info.SubstringByTextElements(0);
                if (calc_flow_alpha == "0") Calc_flow.Text = "";
            }
            catch { }
            try
            {
                System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_sub = si.SubstringByTextElements(Calc_flow.Text.Length - 1);
                if (calc_flow_sub != "-") Calc_flow.Text += "-";
            }
            catch { }
            if (operatorPressed == true) return;
            if (Calc_flow.Text.Length == 0 && Calc.Text.Length == 0)
            {
                return;
            }
            if (Calc_flow.Text.Length != 0)
            {
                try
                {
                    if(alreadyCalc == false)
                    {
                        string temp = Calc_flow.Text;
                        Calc_flow.Text = Calc_flow.Text.Remove(0);
                        Calc_flow.Text = temp + Calc.Text;
                    }
                }
                catch { }
            }
            try
            {
                received_fomula = Convert.ToDouble(Calc.Text);
                if (Calc_flow.Text.Length == 0)
                {
                    current_result = received_fomula;
                    if(alreadyCalc == false)Calc_flow.Text += Calc.Text + "-";
                }
                else if (equal == false)
                {
                    current_result -= received_fomula;
                    Calc_flow.Text += "-";
                }
            }
            catch
            {
                Calc_flow.Text += "-";
            }
            try
            {
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
            operatorPressed = true;
            equal = false;
            count = 0;
        }

        private void Multiplication_btn_Click(object sender, EventArgs e)
        {
            bool alreadyCalc = false;
            calc_mode = 2;
            if (equal == true)
            {
                Calc_flow.Text = "";
                Calc_flow.Text = Convert.ToString(current_result);
                alreadyCalc = true;
            }
            try
            {
                System.Globalization.StringInfo info = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_alpha = info.SubstringByTextElements(0);
                if (calc_flow_alpha == "0") Calc_flow.Text = "";
            }
            catch { }
            try
            {
                System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_sub = si.SubstringByTextElements(Calc_flow.Text.Length - 1);
                if (calc_flow_sub != "*") Calc_flow.Text += "*";
            }
            catch { }
            if (operatorPressed == true) return;
            if (Calc_flow.Text.Length == 0 && Calc.Text.Length == 0)
            {
                return;
            }
            if (Calc_flow.Text.Length != 0)
            {
                try
                {
                    if(alreadyCalc == false)
                    {
                        string temp = Calc_flow.Text;
                        Calc_flow.Text = Calc_flow.Text.Remove(0);
                        Calc_flow.Text = temp + Calc.Text;
                    }
                }
                catch { }
            }
            try
            {
                received_fomula = Convert.ToDouble(Calc.Text);
                if (Calc_flow.Text.Length == 0)
                {
                    current_result = received_fomula;
                    if(alreadyCalc == false)Calc_flow.Text += Calc.Text + "*";
                }
                else if (equal == false)
                {
                    current_result = current_result * received_fomula;
                    Calc_flow.Text += "*";
                }
            }
            catch
            {
                Calc_flow.Text += "*";
            }
            try
            {
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
            operatorPressed = true;
            equal = false;
            count = 0;
        }

        private void Division_btn_Click(object sender, EventArgs e)
        {
            bool alreadyCalc = false;
            calc_mode = 3;
            if (equal == true)
            {
                Calc_flow.Text = "";
                Calc_flow.Text = Convert.ToString(current_result);
                alreadyCalc = true;
            }
            try
            {
                System.Globalization.StringInfo info = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_alpha = info.SubstringByTextElements(0);
                if (calc_flow_alpha == "0") Calc_flow.Text = "";
            }
            catch { }
            try
            {
                System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_sub = si.SubstringByTextElements(Calc_flow.Text.Length - 1);
                if (calc_flow_sub != "/") Calc_flow.Text += "/";
            }
            catch { }
            if (operatorPressed == true) return;
            if (Calc_flow.Text.Length == 0 && Calc.Text.Length == 0)
            {
                return;
            }
            if (Calc_flow.Text.Length != 0)
            {
                try
                {
                    if(alreadyCalc == false)
                    {
                        string temp = Calc_flow.Text;
                        Calc_flow.Text = Calc_flow.Text.Remove(0);
                        Calc_flow.Text = temp + Calc.Text;
                    }
                }
                catch { }
            }
            try
            {
                received_fomula = Convert.ToDouble(Calc.Text);
                if (Calc_flow.Text.Length == 0)
                {
                    current_result = received_fomula;
                    if(alreadyCalc == false)Calc_flow.Text += Calc.Text + "/";
                }
                else if (equal == false)
                {
                    current_result = current_result / received_fomula;
                    Calc_flow.Text += "/";
                }
            }
            catch
            {
                Calc_flow.Text += "/";
            }
            try
            {
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
            operatorPressed = true;
            equal = false;
            count = 0;
        }

        private void Mod_btn_Click(object sender, EventArgs e)
        {
            bool alreadyCalc = false;
            calc_mode = 4;
            if (equal == true)
            {
                Calc_flow.Text = "";
                Calc_flow.Text = Convert.ToString(current_result);
                alreadyCalc = true;
            }
            try
            {
                System.Globalization.StringInfo info = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_alpha = info.SubstringByTextElements(0);
                if (calc_flow_alpha == "0") Calc_flow.Text = "";
            }
            catch { }
            try
            {
                System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                string calc_flow_sub = si.SubstringByTextElements(Calc_flow.Text.Length);
                if (calc_flow_sub != "%") Calc_flow.Text += "%";
            }
            catch { }
            if (operatorPressed == true) return;
            if (Calc_flow.Text.Length == 0 && Calc.Text.Length == 0)
            {
                return;
            }
            if (Calc_flow.Text.Length != 0)
            {
                try
                {
                    if(alreadyCalc == false)
                    {
                        string temp = Calc_flow.Text;
                        Calc_flow.Text = Calc_flow.Text.Remove(0);
                        Calc_flow.Text = temp + Calc.Text;
                    }
                }
                catch { }
            }
            try
            {
                received_fomula = Convert.ToDouble(Calc.Text);
                if (Calc_flow.Text.Length == 0)
                {
                    current_result = received_fomula;
                    if(alreadyCalc == false)Calc_flow.Text += Calc.Text + "%";
                }
                else if (equal == false)
                {
                    current_result = current_result % received_fomula;
                    Calc_flow.Text += "%";
                }
                current_result = ToRoundDown(current_result, 1);//小数点以下切り捨ては別機能にしてもいいかも
            }
            catch
            {
                Calc_flow.Text += "%";
            }
            try
            {
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
            operatorPressed = true;
            equal = false;
            count = 0;
        }
        */
        #endregion

        /// <summary>
        /// dValueを小数点第iDigits位まで切り捨てる(iDigit位は切り捨てられる)
        /// </summary>
        /// <param name="dValue"></param>
        /// <param name="iDigits"></param>
        /// <returns>小数点第iDigits位まで切り捨てられたdouble dValue</returns>
        public static double ToRoundDown(double dValue, int iDigits)
        {
            double dCoef = System.Math.Pow(10, iDigits);
            return dValue > 0 ? System.Math.Floor(dValue * dCoef) / dCoef :
                                System.Math.Ceiling(dValue * dCoef) / dCoef;
        }

        private void Equal_btn_Click(object sender, EventArgs e)
        {
            if (calc_mode == -1)
            {
                MessageBox.Show("演算子を入力してください");
                return;
            }
            if (equal == false) try { received_fomula = Convert.ToDouble(Calc.Text); } catch { }
            try
            {
                Calc.Text = Calc.Text.Remove(0);
            }
            catch { }
            try
            {
                switch (calc_mode)
                {
                    case 0:
                        if (equal == true) Calc_flow.Text += "+";
                        try
                        {//末尾の演算子が + でなければ、+を追加する
                            System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                            string calc_flow_beta = si.SubstringByTextElements(Calc_flow.Text.Length - 1);
                            if (calc_flow_beta != "+") Calc_flow.Text += "+";
                        }
                        catch { }
                        Calc_flow.Text += received_fomula;
                        current_result += received_fomula;
                        break;
                    case 1:
                        try
                        {
                            System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                            string calc_flow_beta = si.SubstringByTextElements(Calc_flow.Text.Length - 1);
                            if (calc_flow_beta != "-") Calc_flow.Text += "-";
                        }
                        catch { }
                        if (equal == true) Calc_flow.Text += "-";
                        Calc_flow.Text += received_fomula;
                        current_result -= received_fomula;
                        break;
                    case 2:
                        try
                        {
                            System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                            string calc_flow_beta = si.SubstringByTextElements(Calc_flow.Text.Length - 1);
                            if (calc_flow_beta != "*") Calc_flow.Text += "*";
                        }
                        catch { }
                        if (equal == true) Calc_flow.Text += "*";
                        Calc_flow.Text += received_fomula;
                        current_result *= received_fomula;
                        break;
                    case 3:
                        try
                        {
                            System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                            string calc_flow_beta = si.SubstringByTextElements(Calc_flow.Text.Length - 1);
                            if (calc_flow_beta != "/") Calc_flow.Text += "/";
                        }
                        catch { }
                        if (equal == true) Calc_flow.Text += "/";
                        Calc_flow.Text += received_fomula;
                        current_result /= received_fomula;
                        break;
                    case 4:
                        try
                        {
                            System.Globalization.StringInfo si = new System.Globalization.StringInfo(Calc_flow.Text);
                            string calc_flow_beta = si.SubstringByTextElements(Calc_flow.Text.Length - 1);
                            if (calc_flow_beta != "%") Calc_flow.Text += "%";
                        }
                        catch { }
                        if (equal == true) Calc_flow.Text += "%";
                        Calc_flow.Text += received_fomula;
                        current_result = current_result % received_fomula;
                        //current_result = ToRoundDown(current_result, 0);//別途、切り捨てボタン追加しよう
                        break;
                    default:
                        break;
                }
                Calc.Text = Convert.ToString(current_result);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            equal = true;
            operatorPressed = false;
        }
    }
}

#region ToDoList

/*
 * 1.改善案
 * 
 * i)ユーザーが計算式をコピペできるようにする
 * 
 * 方法1)
 * 大まかな流れ
 * 計算式ペースト
 * 計算式貼り付けウィンドウ表示(別フォーム？いいえ、計算式貼り付けボタン押下->CTRL+Vで貼り付け(ペースト待機中は窓に待機中表示&計算式貼り付けボタンをキャンセルボタンにする))
 * 文字置き換えバリエーション対応(スペースはすべて取り除く -> 掛け算と割り算の置き換え)
 * イコールボタン押下で計算
 * 
 * 方法2)
 * RPN parserを使う
 * ボタン押下でクリップボードから計算式取得 -> 計算まで一気にやる
 * 
 * 
 * 
 * ii)+-入れ替え機能
 * 
 * 数字入力後、+-ボタン押下で正負入れ替え
 * -(マイナス)をかけるといけそう
 * 
 * 
 * 
 * コード整理をしよう
 * 
 * 計算はクラス化しよう!
 * 
 * 
 * 2.バグ
 * 
 * 0除算を排除すること!
 * 今は∞と出る
 * 
 * 
 */

#endregion